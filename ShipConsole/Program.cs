﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShipLib;

namespace ShipConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Choose a ship type: Cruise or Cargo \n");
            string shiptype = Console.ReadLine();
            Console.WriteLine("Enter the name of the ship:");
            string name = Console.ReadLine();
            Console.WriteLine("Enter the type of the ship:");
            string type = Console.ReadLine();
            Console.WriteLine("Enter the home_port of the ship:");
            string home_port = Console.ReadLine();
            Console.WriteLine("Enter the tonnage of the ship:");
            string tonnage = Console.ReadLine();
            Console.WriteLine("Enter the PIP_captain of the ship:");
            string PIP_captain = Console.ReadLine();
            Console.WriteLine("Enter the members_num of the ship:");
            string members_num = Console.ReadLine();
            Console.WriteLine("Enter the spillway of the ship:");
            string spillway = Console.ReadLine();
            Console.WriteLine("Enter the power of the ship:");
            string power = Console.ReadLine();
            Console.WriteLine("Enter the speed of the ship:");
            string speed = Console.ReadLine();

            switch (shiptype)
            {
                case "Cruise":
                    Console.WriteLine("Enter the occupied_seats of the ship:");
                    string occupied_seats = Console.ReadLine();
                    Console.WriteLine("Enter the free_seats of the ship:");
                    string free_seats = Console.ReadLine();
                    Cruise_ship mycruiseship = new Cruise_ship();
                    mycruiseship.name = name;
                    mycruiseship.type = type;
                    mycruiseship.home_port = home_port;
                    mycruiseship.tonnage = Convert.ToInt32 (tonnage);
                    mycruiseship.PIP_captain = PIP_captain;
                    mycruiseship.members_num = Convert.ToInt32(members_num);
                    mycruiseship.spillway = spillway;
                    mycruiseship.power = Convert.ToInt32(power);
                    //mycruiseship.speed = Convert.ToInt32(speed);
                    mycruiseship.occupied_seats = Convert.ToInt32(occupied_seats); 
                    mycruiseship.free_seats = Convert.ToInt32(free_seats);

                    mycruiseship.changespeed(Convert.ToInt32(speed));

                    Console.WriteLine($"\nInformation about {mycruiseship.name}:\nType:{mycruiseship.type};\nHome port:{mycruiseship.home_port};" +
                        $"\nTonnage:{mycruiseship.tonnage};P.I.P. of the captain:\n{mycruiseship.PIP_captain};\nNumber of crew members:{mycruiseship.members_num};" +
                        $"\nLaunch date:{mycruiseship.spillway};\nEngine power:{mycruiseship.power};\nCurrent speed:{mycruiseship.speed};" +
                        $"\nThe number of occupied seats{mycruiseship.occupied_seats};\nNumber of free seats{mycruiseship.free_seats};");
                    
                    break;

                case "Cargo":
                    Console.WriteLine("Enter the cargo_type of the ship:");
                    string cargo_type = Console.ReadLine();
                    Console.WriteLine("Enter the load_capacity of the ship:");
                    string load_capacity = Console.ReadLine();
                    Cargo_ship mycargoship = new Cargo_ship();
                    mycargoship.name = name;
                    mycargoship.type = type;
                    mycargoship.home_port = home_port;
                    mycargoship.tonnage = Convert.ToInt32(tonnage);
                    mycargoship.PIP_captain = PIP_captain;
                    mycargoship.members_num = Convert.ToInt32(members_num);
                    mycargoship.spillway = spillway;
                    mycargoship.power = Convert.ToInt32(power);
                    //mycargoship.speed = Convert.ToInt32(speed);
                    mycargoship.cargo_type = cargo_type;
                    mycargoship.load_capacity = Convert.ToInt32(load_capacity);

                    mycargoship.changespeed(Convert.ToInt32(speed));
                    
                    Console.WriteLine($"\nInformation about {mycargoship.name}:\nType:{mycargoship.type};\nHome port:{mycargoship.home_port};" +
                        $"\nTonnage:{mycargoship.tonnage};\nP.I.P. of the captain:{mycargoship.PIP_captain};\nNumber of crew members:{mycargoship.members_num};" +
                        $"\nLaunch date:{mycargoship.spillway};\nEngine power:{mycargoship.power};\nCurrent speed:{mycargoship.speed};" +
                        $"\nType of cargo:{mycargoship.cargo_type};\nLoad capacity{mycargoship.load_capacity};");
                    
                    break;
            }
            Console.ReadKey();
        }
    }
}
