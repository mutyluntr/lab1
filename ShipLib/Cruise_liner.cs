﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShipLib
{
    public class Cruise_ship : IShip
    {
        public string name { get; set; }
        public string type { get; set; }
        public string home_port { get; set; }
        public int tonnage { get; set; }
        public string PIP_captain { get; set; }
        public int members_num { get; set; }
        public string spillway { get; set; }
        public int power { get; set; }
        public int speed { get; set; }
        public void changespeed(int speedc)
        {
            this.speed = speedc;
        }

        public int occupied_seats { get; set; }
        public int free_seats { get; set; }
    }
}
