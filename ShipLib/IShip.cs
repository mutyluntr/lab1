﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShipLib
{
    public interface IShip
    {
         string name { get; set; }
         string type { get; set; }
         string home_port { get; set; }
         int tonnage { get; set; }
         string PIP_captain { get; set; }
         int members_num { get; set; }
         string spillway { get; set; }
         int power { get; set; }
         int speed { get; set; }
        void changespeed(int speedc);

    }
}
